require("environments.Environment")

local __allEnvironments = {
   ["pd"] = "PrisonersDilemma",
   ["pp"] = "PursuitProblem",
}

function allEnvironments()
   local _allEnvironments = {}
   for environmentName, _ in pairs(__allEnvironments) do
      table.insert(_allEnvironments, environmentName)
   end
   return _allEnvironments
end

function getEnvironment(environmentName)
   assert(__allEnvironments[environmentName] ~= nil, "Environment not found!")

   require("environments." .. __allEnvironments[environmentName])

   -- Remember the old value
   local __oldVal = __EnvironmentType

   -- Put environment type in global variable __EnvironmentType
   loadstring("__EnvironmentType = " .. __allEnvironments[environmentName])()

   -- Copy environment type to local _EnvironmentType
   local _EnvironmentType = __EnvironmentType

   -- Put back the old value of global varialbe __EnvironmentType
   __EnvironmentType = __oldVal

   -- Return the value of local _EnvironmentType
   return _EnvironmentType
end
