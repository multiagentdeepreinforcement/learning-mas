require("torch")

local PrisonersDilemma, Parent = torch.class("PrisonersDilemma", "Environment")

function PrisonersDilemma:__init(opts)

   opts = opts or {}

   self.noAssertions = opts.noAssertions or false

   self.name = "PD Env"

   Parent.__init(self, opts)

   print("ASDASDASD")
   self.agentsNo = opts.agentsNo or 2
   assert(self.agentsNo == 2, "Only two agents are supported")

   self.coop_coop     =  -2 or opts.coop_coop
   self.coop_betray   = -10 or opts.coop_betray
   self.betray_coop   =   0 or opts.betray_coop
   self.betray_betray =  -5 or opts.betray_betray

   assert(self.coop_coop   < self.betray_coop)
   assert(self.coop_betray < self.betray_betray)
   
   self.rewards = {
      ["C"] = {
         ["C"] = { self.coop_coop    , self.coop_coop     },
         ["B"] = { self.coop_betray  , self.betray_coop   },
      },
      ["B"] = {
         ["C"] = { self.betray_coop  , self.coop_betray   },
         ["B"] = { self.betray_betray, self.betray_betray },
      },
   }

   self:info("Prisoner's Dilemma environment is initialized")
end

function PrisonersDilemma:restart()
   return {}
end

function PrisonersDilemma:availableActions(agent)
   if agent then
      assert((agent == 1) or (agent == 2), "Who is " .. tostring(agent) .. "?")
      return {"C", "B"}
   else
      return {{"C", "B"}, {"C", "B"}}
   end
end

function PrisonersDilemma:applyActions(actions)
   assert(
      actions[1] and actions[2],
      "Prisoner's Dilemma must be played in a synchronized manner"
   )

   local lastActions = {[1] = actions[2], [2] = actions[1]}
   local rewards     = self.rewards[actions[1]][actions[2]]

   return lastActions, rewards
end
