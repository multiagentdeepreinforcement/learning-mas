require("torch")

local Environment = torch.class("Environment")


-- Initalizes general stuff in the environment
function Environment:__init(opts)
   opts = opts or {}
   self:info("Initialized abstract environment")
end

-- Restarts / initializes environment
-- it returns the initial observations and if final state
function Environment:restart(seed)
   assert(false, "Virtual method")
end


-- It returns the available actions for all agents
function Environment:availableActions(agent)
   assert(false, "Virtual method")
end

-- It returns environment moves count
function Environment:crtMoveNo()
   assert(false, "Virtual method")
end

-- It returns current environment game number
function Environment:crtGameNo()
   assert(false, "Virtual method")
end

function Environment:getStateRepresentation()
   return {}
end

-- Applies the actions chosen by the agents
-- Must return 4xobj ::
-- state (obj, observation), reward (for each agent), ...
-- done (bool, if it's time to reset env), inf (dict, diagnostic info)
function Environment:applyActions(actions)
   assert(false, "Virtual method")
end


function Environment:info(...)
   info(self.name or "ENV", ...)
end

function Environment:debug(...)
   dinfo(self.name or "ENV", ...)
end
