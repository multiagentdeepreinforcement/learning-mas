--
-- User: andrei
-- Date: 12/8/16
-- Time: 8:10 PM
--
require 'image'
require("torch")

local PursuitProblem, Parent = torch.class("PursuitProblem", "Environment")

function PursuitProblem:__init(opts)

    opts = opts or {}

--------------------------------------------------------------------------------
-- Parameters initializations

    self.noAssertions = opts.noAssertions or false

    self.name = "PP Env"

    Parent.__init(self, opts)

    self.agentsNo = opts.agentsNo or 2
    assert((self.agentsNo >= 2) and (self.agentsNo <= 4),
        "Between 2 and 4 predator agents possible")

    --Map
    self.mapSize = opts.mapSize or 6
    assert(self.mapSize >= 5, "Map too small")

    --Max moves
    self.maxMoves = 100 or opts.maxMoves
    assert(self.maxMoves > 1, "Must permin at least 1 move")

    --Prey parameters
    self.preyFOV = opts.preyFOV or 3
    assert((self.preyFOV >= 3) and (self.preyFOV % 2 == 1), "preyFOV error")

    self.preyValidMoves = opts.preyValidMoves or
        {{0,0}, {-1,0}, {1,0}, {0,-1}, {0,1},  {-1,-1}, {-1,1}, {1, 1}, {1,-1}}

    --Agents (Predator) parameters
    self.predatorFOV = opts.predatorFOV or 5
    assert((self.predatorFOV >= 3) and (self.predatorFOV % 2 == 1), 
        "Predator FOV error")

    --Agents Valid moves &
    self.predatorValidMoves = opts.predatorValidMoves or 
        {{0,0}, {-1,0}, {1,0}, {0,-1}, {0,1}}
    assert(#self.predatorValidMoves > 0, "No valid predator moves")

    --Reward
    self.rewardType = opts.rewardType or "SUR"

    self.reward = 1 or opts.reward

    --Winning positions
    self.agentsWinPos = opts.winningPositions or {{-1,0}, {1,0}, {0,-1}, {0,1}}

    --State representation
    self.stateRepresentation = opts.stateRepresentation or "ID_MAPS"

    --initDisplay properties
    self.drawGame = opts.drawGame or false

--------------------------------------------------------------------------------
-- Initalize environment
    self.gameNo         = 0

    self.agentsWinPos   = torch.Tensor(self.agentsWinPos):int()

    self.prey           = {
                            fov = self.preyFOV,
                            position = torch.Tensor({ -1, -1 }):int(),
                            id = -1
                        }
    self.prey.maxDist   = math.floor(self.prey.fov/2)
    self.preyValidMoves = torch.Tensor(self.preyValidMoves):int()

    self.predatorMaxDist= math.floor(self.predatorFOV/2)
    self.predatorValidMoves = torch.Tensor(self.predatorValidMoves):int()
    self.predators      = {}

    --AGENTS WILL always have positive numbers allocated as ID (between 1-agNo)
    --*Important as this is assumed in the following env implementation
    for i = 1, self.agentsNo do
        local p = { position = {-1,-1}, fov = self.predatorFOV,
                    maxDist = self.predatorMaxDist, id = i}
        table.insert(self.predators, p)
    end

    --map Representation
    self.mapEmptyID     = 0
    self.mapWallID      = -2
    self.mapPadding     = math.max(self.prey.maxDist, self.predatorMaxDist)
    self.realMinMapIx   = self.mapPadding + 1
    self.realMaxMapIx   = self.mapPadding + self.mapSize
    self.localMapSize   = self.mapSize + self.mapPadding * 2
    self.map            = torch.zeros(self.localMapSize, 
                            self.localMapSize):int()


    if self.rewardType == "SUR" then
        self.getPredatorReward = function (...) 
            return self.getSURReward(...) end
    else
        assert(false, "No reward type set. Got: " .. self.rewardType)
    end

    if self.stateRepresentation == "BIM_MAPS" then
        self.getPredatorState = function (...) return self.BIMMapsState(...) end
    elseif self.stateRepresentation == "ID_MAPS" then
        self.getPredatorState = function (...) return self.IDMapsState(...) end
    else
        assert(false, "No state representaion set. Got: " 
            .. self.stateRepresentation)
    end

    if self.drawGame then
        self:initDrawGameSettings()
    end

    self:restart()

    self:info("Pursuit problem environment is initialized")
end

--------------------------------------------------------------------------------
-- Map / game utils

function PursuitProblem:initiateMap()
    self.map:fill(self.mapWallID)
    self.map:narrow(1, self.realMinMapIx, self.mapSize)
        :narrow(2, self.realMinMapIx, self.mapSize):fill(self.mapEmptyID)

    --Position Prey
    local emptyPositions = torch.range(1, self.mapSize * self.mapSize)
    local newPos = emptyPositions[torch.random(1, emptyPositions:nElement())]
    emptyPositions = emptyPositions[emptyPositions:ne(newPos)]
    self.prey.position = torch.Tensor({
            math.floor((newPos - 1) / self.mapSize) + 1 + self.mapPadding,
            math.floor((newPos - 1) % self.mapSize) + 1 + self.mapPadding
        }):int()

    self.map[{self.prey.position[1], self.prey.position[2]}]  = self.prey.id

    --Position Predators
    for i = 1, self.agentsNo do
        local newPos= emptyPositions[torch.random(1, emptyPositions:nElement())]
        emptyPositions = emptyPositions[emptyPositions:ne(newPos)]
        self.predators[i].position = torch.Tensor({
            math.floor((newPos - 1) / self.mapSize) + 1 + self.mapPadding,
            math.floor((newPos - 1) % self.mapSize) + 1 + self.mapPadding
        }):int()
        self.map[{self.predators[i].position[1],
            self.predators[i].position[2]}] = i
    end
end

function PursuitProblem:restart()
    self.gameNo     = self.gameNo + 1
    self.moveNo     = 0
    self.wonGame    = false
    self:initiateMap()

    local newstate, winningState = self:getCurrentState()

    -- Restart game until not in a end game state 
    while winningState do
        self:initiateMap()
        newstate, winningState = self:getCurrentState()
    end

    return newstate, winningState
end

function PursuitProblem:availableActions(agent)
    if agent then
        local validAgent = false
        for i = 1, self.agentsNo do
            validAgent = validAgent or (agent == i)
        end
        assert(validAgent, "Who is " .. tostring(agent) .. "?")
        return self.predatorValidMoves
    else
        return self.predatorValidMoves
    end
end

-- It returns if the environment has reached the maximum number of moves
-- permited
function Environment:finishedGame()
end

-- It returns environment moves count
function Environment:crtMoveNo()
    return self.moveNo
end

function Environment:crtGameNo()
   return self.gameNo
end




--------------------------------------------------------------------------------
-- Prey move actions

function PursuitProblem:preyAction()
    local closePredatorsPositions = {}

    for i = 1, self.agentsNo do
        local distP = self.predators[i].position - self.prey.position
        if distP:abs():le(self.prey.maxDist):all() then
                table.insert(closePredatorsPositions,
                    self.predators[i].position)
        end
    end

    local maxDistFromPredators = 0
    local maxMove = -1
    for i = 1, self.preyValidMoves:size(1) do
        local dMove = 0
        local possibleMove = (self.prey.position + self.preyValidMoves[i])

        if self.map[{possibleMove[1], possibleMove[2]}] == self.mapEmptyID then
            for j = 1, #closePredatorsPositions do
                dMove = dMove + (possibleMove -
                        closePredatorsPositions[j]):abs():sum()
            end
            if dMove > maxDistFromPredators then
                maxDistFromPredators = dMove
                maxMove = i
            end
        end
    end

    if maxMove > 0 then
        self.map[{self.prey.position[1], 
            self.prey.position[2]}] = self.mapEmptyID
        self.prey.position = (self.prey.position + self.preyValidMoves[maxMove])
        self.map[{self.prey.position[1], self.prey.position[2]}] = self.prey.id
    end
end

--------------------------------------------------------------------------------
-- State representations

--Get perceived information
--[[
--1.    [BIM_MAPS] Binary individual map State representation
        -- :: 3x{FOVxFOV} prey position; wall position; predators;
        -- 1 - for occupied position; 0 elsewhere;
        -- The maps are centered on his position
 ]]
function PursuitProblem:BIMMapsState(predator)
    local idMap = self.map:clone()
    
    idMap[{predator.position[1], predator.position[2]}] = self.mapEmptyID
    idMap = idMap:narrow(1, predator.position[1]-predator.maxDist, predator.fov)
        :narrow(2, predator.position[2]-predator.maxDist, predator.fov)

    local res = torch.Tensor(3, idMap:size(1), idMap:size(2))

    res[1] = idMap:eq(self.prey.id)
    res[2] = idMap:eq(self.mapWallID)
    res[3] = idMap[idMap:ge(1)]

    return res
end

--Get perceived information
--[[
--1.    [ID_MAPS] Binary individual map State representation
        -- :: 1x{FOVxFOV}
        -- -1 wall, 0 empty, 1 predators, 2 prey
 ]]
function PursuitProblem:IDMapsState(predator)
    local idMap = self.map:clone()
    
    idMap[{predator.position[1], predator.position[2]}] = self.mapEmptyID
    idMap = idMap:narrow(1, predator.position[1]-predator.maxDist, predator.fov)
        :narrow(2, predator.position[2]-predator.maxDist, predator.fov)

    idMap[idMap:ge(1)] = 1
    idMap[idMap:eq(self.mapWallID)] = -1
    idMap[idMap:eq(self.mapEmptyID)] = 0
    idMap[idMap:eq(self.prey.id)] = 2

    idMap = idMap:reshape(1, idMap:size(1), idMap:size(2))
    return idMap
end

-- Get state represetation format for each type
function PursuitProblem:getStateRepresentation()
    local stateRepresentation = {}
    if self.stateRepresentation == "BIM_MAPS" then
        stateRepresentation = torch.Tensor(3, self.predatorFOV,
            self.predatorFOV)
    elseif self.stateRepresentation == "ID_MAPS" then
        stateRepresentation = torch.Tensor(1, self.predatorFOV,
            self.predatorFOV)
    end

    return stateRepresentation
end

function PursuitProblem:getCurrentState()

    if self.drawGame then
        self:drawGameImage()
    end

    local res = {}
    if self.getPredatorState then
        for i = 1, self.agentsNo do
            table.insert(res, self:getPredatorState(self.predators[i]))
        end
    end

    --Check if Winning game state
    local predatorsIdWinning = {}
    for i = 1, self.agentsWinPos:size(1) do
        local pos = self.agentsWinPos[i] + self.prey.position
        if self.map[{pos[1], pos[2]}] >= 1 then
            table.insert(predatorsIdWinning, self.map[{pos[1], pos[2]}])
        end
    end

    if #predatorsIdWinning == self.agentsNo then
        self.wonGame = true
    else
        self.wonGame = false
    end

    return res, self.wonGame
end

--------------------------------------------------------------------------------
-- Reward functions

-- Rewards
--[[
-- 1.   [SUR] Single uniform reward
        -- Get same reward only when all agents have occupied winning
        -- positions around the Prey
        -- Reward is calculated [0,1] 0 - reached maxMoves
 ]]
--
function PursuitProblem:getSURReward(predator)
    local predatorReward = 0

    --JUST test reward if close to prey
    local distP = predator.position - self.prey.position
    if distP:abs():le(predator.maxDist):all() then
        predatorReward =  predatorReward + 
                            (1.0 - (distP:abs():sum()/ (predator.maxDist*2)))

    end

    if self.wonGame then
        predatorReward = (1 + ((self.maxMoves * self.reward -
                self.moveNo * self.reward)/self.maxMoves))
    end
    return predatorReward
end

function PursuitProblem:getReward()
    local allRewards = {}
    if self.getPredatorReward then
        for i = 1, self.agentsNo do
            table.insert(allRewards, self:getPredatorReward(self.predators[i]))
        end
    end
    return allRewards
end

--------------------------------------------------------------------------------
-- Draw visual maps

function PursuitProblem:drawGameImage()
    local display = image.scale(torch.Tensor(
        self.map
        :narrow(1, self.mapPadding+1, self.mapSize)
        :narrow(2, self.mapPadding+1, self.mapSize)
        :double()),
        self._drawGame_displaySize, self._drawGame_displaySize, 'simple')

    self._display_Image:copy(self._display_Image_empty)
    for i = 1, self._display_Image:size(1) do
        self._display_Image[i][display:eq(self.prey.id)]
            = self._drawGame_preyColor[i]
        for j = 1, self.agentsNo do
            self._display_Image[i]
                    [display:eq(j)] = self._drawGame_predatorColors[j][i]
        end
    end
    self._display_win = self._display.image(self._display_Image,
        {win=self._display_win, title='Game Move no.:' .. self.moveNo})

end

function PursuitProblem:initDrawGameSettings()
    self._display = require 'display'
    self._drawGame_displaySize      = 256
    self._drawGame_emptyColors      = {{ 250, 250, 250 }, {220,220,220}}-- B,G,R
    self._drawGame_preyColor        = { 0, 250, 0 } -- B,G,R
    self._drawGame_predatorColors   = { {0, 250, 250},
                                        {250, 10, 10},   -- B,G,R
                                        {250, 250, 10} } -- B,G,R
    --chessboard like map bg
    self._display_Image_empty = torch.Tensor(3,
                        self.mapSize,
                        self.mapSize)
    local posCnt = 1
    for j=1,self._display_Image_empty:size(2) do
        for k=1,self._display_Image_empty:size(3) do
            self._display_Image_empty[{{1,3}, j, k}] = 
                torch.Tensor(self._drawGame_emptyColors[(j+k) % 2 + 1])
            posCnt = posCnt + 1
        end
    end

    self._display_Image_empty = image.scale(self._display_Image_empty,
        self._drawGame_displaySize, self._drawGame_displaySize, 'simple')

    self._display_Image = torch.Tensor(3,
                        self._drawGame_displaySize,
                        self._drawGame_displaySize):fill(0)

    self._display_win = self._display.image(self._display_Image,
        {win=self._display_win, title='Game Move no.:' .. 0})
end

--------------------------------------------------------------------------------
-- Gameplay implementation

function PursuitProblem:applyActions(actions)

    local validActions = true
    for i = 1, self.agentsNo do
        validActions = validActions and (actions[i])
    end

    assert(validActions,
        "Predator prey must be played in a synchronized manner")

    self.moveNo = self.moveNo + 1

    --Prey take action
    self:preyAction()

    --Predator take action
    for i = 1, self.agentsNo do
        local newPos = self.predators[i].position + actions[i]
        if self.map[{newPos[1], newPos[2]}] == self.mapEmptyID then
            self.map[{self.predators[i].position[1],
                self.predators[i].position[2]}] = self.mapEmptyID
            self.predators[i].position = newPos
            self.map[{self.predators[i].position[1],
                self.predators[i].position[2]}] = i
        end
    end

    --Game finished check (Exceeded maxMoves / game won)
    local newstate, winningState = self:getCurrentState()
    local rewards = self:getReward()
    local done = (self.moveNo > self.maxMoves) or self.wonGame

    local info = {}

    if done then
        if self.moveNo > self.maxMoves then
            info.doneInfo = "Exceeded maxMoves"
        else
            info.doneInfo = "Game won"
        end
    end

    return newstate, rewards, done, info
end