require("torch")

local cmd = torch.CmdLine()

cmd:text()
cmd:text("Train multi-agent systems using Deep Reinforcement Learning")
cmd:text()

-- Overwrite arguments from file
cmd:option("--argF", "", "Load arguments from JSON file & overwrite")

-- General options
cmd:option("--verbose", 1, "Verbosity level")
cmd:option("--noAssertions", false, "No assertions - might speed up things")

cmd:option("--saveFolder", "experiments", "Base folder for experiments")
cmd:option("--savePrefix", "", "Log folder prefix")

cmd:option("--drawGame", true, "Draw game map (http://localhost:8000/)")

-- Torch setup options
cmd:option("--seed", 1, "Seed to start the random number generators")

--------------------------------------------------------------------------------
-- Environment options
cmd:text()
cmd:text()
cmd:text("Environment options")
cmd:text()


cmd:option("--environment", "pp", "Environment to be used")
cmd:option("--agentsNo", 2, "Total number of agents")


-- -- PP Environment options
cmd:text()
cmd:text("PP Environment options")

cmd:option("--mapSize", 6, "Map size")
cmd:option("--maxMoves", 100, "Map maximum no of moves per game")
cmd:option("--winningPositions",
           "{{-1,0}, {1,0}, {0,-1}, {0,1}}",
           "Game winning positions in accordance with prey")

cmd:option("--preyFov", 3, "Prey field of view (*odd no)")
cmd:option("--preyValidMoves",
           "{{0,0},{-1,0},{1,0},{0,-1},{0,1},{-1,-1},{-1,1},{1, 1},{1,-1}}",
           "Table of valid prey moves")

cmd:option("--predatorFOV", 5, "Predator field of view (*odd no)")
cmd:option("--predatorValidMoves",
           "{{0,0}, {-1,0}, {1,0}, {0,-1}, {0,1}}",
           "Table of valid predator moves")

cmd:option("--rewardType", "SUR", "Rewarding system type")
cmd:option("--reward", 1, "Reward factor")

cmd:option("--stateRepresentation", "ID_MAPS",
	"State representation system type")

--------------------------------------------------------------------------------
-- Agents & training options

cmd:text()
cmd:text()
cmd:text("Agents & training options")
cmd:text()

-- Agent options
cmd:option("--agents", "bqnn:2", "type1:n1 type2:n2 etc..")

-- Reporting
cmd:option("--reportEvery", 100, "Report statistics every n steps")
cmd:option("--saveModelFreq", 2, "Epoch frequency for saving model")

-- Training options
cmd:option("--sharedModel", true, "Create shared model for all agents")
cmd:option("--totalSteps", 1000000, "Total number of training steps")
cmd:option("--epochSize",   500, "Size of a training epoch")

cmd:option("--modelDataType", "float", "Model data type (eg. float, cuda ..)")

-- -- BQNNAgent options [Uses ID_MAPS]
--------------------------------------------------------------------------------

cmd:text()
cmd:text("BQNNAgent  options")

cmd:option("--learningRate", 1e-1, "Learning rate")
cmd:option("--learningRateDecay", 1e-3, "Learning Rate decay")
cmd:option("--weightDecay", 0, "Weight decay")
cmd:option("--discount", 0, "Future reward discount factor")
cmd:option("--epsilon", 1, "Start prob. of choosing random action")
cmd:option("--epsilonMinimumValue", 0.001, "Min prob. of choosing random act")


--------------------------------------------------------------------------------

function overwriteArguments(opts, argJsonFilePath)
	local json = require("json")

	if path.exists(opts.argF or "") then
            local f = assert(io.open(opts.argF, "rb"))
            local content = f:read("*all")
            f:close()
            local newArg = json.decode(content)
            for key, value in pairs(newArg) do
              if opts[key] then
                 opts[key] = value
              end
           end
	end
	return opts
end

return cmd

git filter-branch --commit-filter '
    if [ "$GIT_COMMITTER_NAME" = "andrei" ];
    then
            GIT_COMMITTER_NAME="andreicnica";
            GIT_AUTHOR_NAME="andreicnica";
            GIT_COMMITTER_EMAIL="andreic.nica@gmail.com";
            GIT_AUTHOR_EMAIL="johnpc@email.domain";
            git commit-tree "$@";
    else
            git commit-tree "$@";
    fi' HEAD
