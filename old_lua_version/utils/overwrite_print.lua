require("torch")
require("string.color")

oldPrint = print

function myPrint(infoLevel, sender, ...)
   if infoLevel <= verbose then
      io.write(string.color("[" .. sender .. "] ", "green"))
      oldPrint(...)
   end
end

print = myPrint

function  info(sender, ...) print(1, sender, ...) end
function dinfo(sender, ...) print(2, sender, ...) end

function mainInfo(...)   info("main", ...) end
function mainDebug(...) dinfo("main", ...) end
