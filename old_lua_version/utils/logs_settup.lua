function initLogs(opts)
        local json = require("json")

        --Generate logs folder
        local rundir = paths.concat(opts.saveFolder,
           ((#opts.savePrefix > 0) and (opts.savePrefix ..  "_") or "")
           .. tostring(opts.environment) .. "_"
           .. tostring(opts.agents)
           )
        opts.rundir = rundir
        paths.mkdir(rundir)

        local tableFields = {
           "winningPositions", "preyValidMoves", "predatorValidMoves"
        }

        for _, field in ipairs(tableFields) do
           opts[field] = loadstring("return " .. field)()
        end

        opts.winningPositions = loadstring("return " .. opts.winningPositions)()
        opts.preyValidMoves = loadstring("return " .. opts.preyValidMoves)()
        opts.predatorValidMoves =
           loadstring("return " .. opts.predatorValidMoves)()

        -- Save arguments to json file
        local serializeArgs = json.encode( opts )
        local file = assert(io.open(opts.rundir .. "/arguments.json", "w"))
        file:write(serializeArgs)
        file:close()

        return opts
end
