# Deep Reinforcement Learning in MAS

Our mission: *Design individual learning agents that discover
collaborative techniques through deep reinforcement learning
techniques.*

Necessary package for displaying images
	luarocks install LuaSocket
	luarocks install https://raw.githubusercontent.com/szym/display/master/display-scm-0.rockspec

	Start server with: 
		th -ldisplay.start 8000 0.0.0.0

	Then open (link) in your browser to load the remote desktop
		http://localhost:8000/


