require("torch")
require("utils.logs_settup")

local cmd   = require("utils.cmd_options")
local opts  = cmd:parse(arg or {})
opts        = overwriteArguments(opts, opts.argF)
opts        = initLogs(opts)
verbose     = opts.verbose or 1

cmd:log(paths.concat(opts.rundir,'out.log'), opts)

require("utils.overwrite_print")

--------------------------------------------------------------------------------
-- Initalize environment
require("environments/all_environments")
local env = getEnvironment(opts.environment)(opts)

--------------------------------------------------------------------------------
-- Initialize agents
require("agents/all_agents")
local agentTypes = utils.split(opts.agents, ",")
local agents = {}
local id = 1
for _, agentType in ipairs(agentTypes) do
   local _info = utils.split(agentType, ":")
   if #_info == 1 then
      agents[id] = getAgent(_info[1])(id, env, opts)
      id = id + 1
   else
      for i = 1, tonumber(_info[2]) do
         agents[id] = getAgent(_info[1])(id, env, opts)
         id = id + 1
      end
   end
end

assert(#agents == opts.agentsNo)
local agentsNo = opts.agentsNo

--------------------------------------------------------------------------------
-- Variables

local totalRewards = {}
local epochReward  = {}

local totalSteps   = opts.totalSteps
local epochSize    = opts.epochSize

local epochsNo     = totalSteps / epochSize
local crtEpoch     = 1

local avgRewards   = torch.zeros(agentsNo, epochsNo)
local avgMovesPerGame = torch.zeros(epochsNo)
local epochGameCnt = 0

--------------------------------------------------------------------------------
-- Various functions

function report(step)
   local crtEpochStep = step - (crtEpoch - 1) * epochSize
   mainInfo("---------------------------")
   for id, _ in ipairs(agents) do
      mainInfo(
         string.color(
            "[Epoch " .. crtEpoch .. ", Step " .. crtEpochStep .. "] ",
            "red"
         )
         .. "Current epoch avg. reward for agent "
            .. string.color(tostring(id), "yellow")
            .. " is "
            .. string.color(
               string.format("%.2f", avgRewards[{id, crtEpoch}] / crtEpochStep),
               "yellow")
      )      
   end
   mainInfo(
      string.color(
         "[Epoch " .. crtEpoch .. ", Step " .. crtEpochStep .. "] ",
         "red"
      )
      .. "Current epoch avg. moves per game for "
         .. string.color(tostring(epochGameCnt), "yellow")
         .. " games is "
         .. string.color(
            string.format("%.2f", avgMovesPerGame[crtEpoch] / 
            (epochGameCnt), "blue"))
   )

end

function restartGame()
   local states, done = env:restart()
   local rewards = {}
   for id, agent in ipairs(agents) do
      rewards[id] = 0
      agent:restart()
   end
   return states, rewards, done
end

--------------------------------------------------------------------------------
-- Train

local states, rewards
local envActionInfo
local done

states, rewards, done = restartGame()

for step = 1, totalSteps do
   -- Agents choose their actions
   local actions = {}
   for id, agent in ipairs(agents) do
      actions[id] = agent:chooseAction(states[id], rewards[id], done)
   end

   -- Actions are being performed in the environment
   states, rewards, done, envActionInfo = env:applyActions(actions)

   -- End of Game
   if done then
      --Send last reward & state to agents (No more moves permitted)
      for id, agent in ipairs(agents) do 
         agent:chooseAction(states[id], rewards[id], done)
      end
      
      avgMovesPerGame[crtEpoch] = avgMovesPerGame[crtEpoch] + env:crtMoveNo()
      epochGameCnt = epochGameCnt + 1
      states, rewards, done = restartGame()
   end

   -- Update various information
   for id, reward in ipairs(rewards) do
      avgRewards[{id, crtEpoch}] = avgRewards[{id, crtEpoch}] + reward
   end

   -- Report (current epoch might not be over)
   if step % opts.reportEvery == 0 then
      report(step)
      for i = 1, agentsNo do agents[i]:report() end
   end

   -- Epoch is over
   if (step % epochSize) == 0 then
      avgRewards[{{}, crtEpoch}]:mul(1/epochSize)
      avgMovesPerGame[crtEpoch] = avgMovesPerGame[crtEpoch] / epochGameCnt
      epochGameCnt = 0
      crtEpoch = crtEpoch + 1

      -- Save Model
      if (crtEpoch % opts.saveModelFreq) == 0 then
         if opts.sharedModel then
            agents[1]:saveModel()
         else
            for i = 1, agentsNo do agents[i]:saveModel() end
         end
      end

   end


end

mainInfo("Done!")

