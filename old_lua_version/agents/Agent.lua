require("torch")

local Agent = torch.class("Agent")

function Agent:__init(agentId, environment, opts)
   opts = opts or {}
   self.noAssertions = opts.noAssertions or false
   self.agentId      = agentId
   self.actions      = environment:availableActions(agentId)
   self.environment  = environment
end

function Agent:chooseAction(newState, previousReward, done)
   assert(false, "Virtual method")
end

--Restarts whenever game is restarted
function Agent:restart()
   do end
end

function Agent:report()
   do end
end

function Agent:saveModel()
   do end
end


function Agent:info(...)
   info(self.name .. " (" .. tostring(self.agentId) ..")", ...)
end

function Agent:debug(...)
   dinfo(self.name .. " (" .. tostring(self.agentId) ..")", ...)
end
