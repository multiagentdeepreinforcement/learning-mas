--
-- Version: 1.0
--

require("torch")

local PPRandomAgent, Parent = torch.class("PPRandomAgent", "Agent")

function PPRandomAgent:__init(agentId, environment, opts)
   opts = opts or {}
   self.name = "BaselinePPRandom"
   Parent.__init(self, agentId, environment, opts)
   self:info("Random agent initialized")
end

function PPRandomAgent:chooseAction(newState, previousReward, done)
   local preyMap = newState[1]:byte()

   local predatorPos = torch.IntTensor({math.floor(preyMap:size(1)/2)+1,
      math.floor(preyMap:size(2)/2)+1})
   local minDist = 314159265
   local idAction = -1
   if preyMap:any() then
      local preyPos = torch.linspace(1, preyMap:nElement(),
         preyMap:nElement())[preyMap:eq(1)][1]
      local row = math.floor((preyPos - 1) / preyMap:size(2)) + 1
      local col = math.floor((preyPos - 1) % preyMap:size(2)) + 1
      preyPos = torch.IntTensor({row, col})
      for _, id in ipairs(torch.randperm(self.actions:size(1)):totable()) do
         local dist = (preyPos - (predatorPos + self.actions[id])):abs():sum()
         if dist < minDist then
            idAction = id
            minDist = dist
         end
      end
   end
   idAction = (idAction == -1 and torch.random(1, self.actions:size(1))
           or idAction)

   return self.actions[idAction]
end
