--
-- Baseline Agent - Q-Learning with Neural networks
-- Version: 1.0
--

require("torch")
require('nn')
require('optim')


local BQNNAgent, Parent = torch.class("BQNNAgent", "Agent")

function BQNNAgent:__init(agentId, environment, opts)
   opts = opts or {}
   self.name = "BQNNAgent"
   Parent.__init(self, agentId, environment, opts)

   self.dataType = opts.dataType or "float"
   self.rundir = opts.rundir or ""

   --Learning parameters
   self.learningRate = opts.learningRate or 1e-1
   self.learningRateDecay = opts.learningRateDecay or 1e-3
   self.weightDecay = opts.weightDecay or 0
   self.momentum = opts.momentum or 0.01

   --Future reward discount factor
   self.discount = opts.discount or 0.9

   --Prob. of choosing random action (in training).
   self.epsilon = opts.epsilon or 1
   self.epsilonMinimumValue = opts.epsilonMinimumValue or 0.001

   self.sharedModel = opts.sharedModel or true

   self.modelPath = paths.concat(
      opts.rundir, 'model_' .. tostring(agentId) .. '.net')
   
   self.stateRepresentation = environment:getStateRepresentation()
   self:createLearningArchitecture()

   self.gameMoves = 0
   self.gameLoss = 0

   self:info("Baseline Agent - Q-Learning with Neural networks")
end

function BQNNAgent:cast(t)
   if self.dataType == 'cuda' then
      require 'cunn'
      return t:cuda()
   elseif self.dataType == 'float' then
      return t:float()
   elseif self.dataType == 'cl' then
      require 'clnn'
      return t:cl()
   else
      error('Unknown type '..opt.type)
   end
end


function BQNNAgent:createLearningArchitecture()
   if self.sharedModel and self.environment.agentsModel then
      self.model = self.environment._agents_model
      self.criterion = self.environment._agents_criterion
      self.optimParams = self.environment._agents_optimParams
      return nil
   end

   self.model = nn.Sequential()
   local inputDepth = self.stateRepresentation:size(1)
   local size = self.stateRepresentation:size(2)
   local actionsNo = self.actions:size(1)
   local nbStates = self.stateRepresentation:nElement()
   local hiddenSize = 100
   self.model:add(nn.SpatialConvolution(1, 128, 3, 3))
   self.model:add(nn.ReLU())
   self.model:add(nn.SpatialConvolution(128, 64, 3, 3))
   self.model:add(nn.ReLU())
   self.model:add(nn.SpatialConvolution(64, actionsNo, 1, 1))
   self.model:add(nn.View(actionsNo))

   --initialize weights
   for k,v in pairs(self.model:findModules(('%s.SpatialConvolution'):format(backend_name))) do
      v.weight:normal(0,0.05)
      v.bias:zero()
   end

   self.model = self:cast(self.model)
   self.criterion = self:cast(nn.MSECriterion())

   self.optimParams = {
      learningRate = self.learningRate,
      learningRateDecay = self.learningRateDecay,
      weightDecay = self.weightDecay,
      momentum = self.momentum,
      dampening = 0,
      nesterov = true
   }


   if self.sharedModel then
      self.environment._agents_model = self.model
      self.environment._agents_criterion = self.criterion
      self.environment._agents_optimParams = self.optimParams
      self.environment._agents_loss = 0
      self.environment._agents_lossRegistered = 0
      self.environment._agents_logger = optim.Logger(paths.concat(
         self.rundir, 'loss_log.txt'))
      self.environment._agents_logger:display(false)
   end

end

function BQNNAgent:restart()
   self.lastActionId = -1
   self.prevState = {}
   self.gameMoves = 0
   self.gameLoss = 0
end


function BQNNAgent:trainNetwork(inputs, targets)
   self.model:training()
   local loss = 0
   local x, gradParameters = self.model:getParameters()
   local function feval(x_new)
      gradParameters:zero()
      local predictions = self.model:forward(inputs)
      local loss = self.criterion:forward(predictions, targets)
      local gradOutput = self.criterion:backward(predictions, targets)
      self.model:backward(inputs, gradOutput)
      return loss, gradParameters
   end

   local _, fs = optim.sgd(feval, x, self.optimParams)
   loss = loss + fs[1]
   return loss
end

function BQNNAgent:forwardPass(input)
   self.model:evaluate()
   local outputs = self.model:forward(input)
   return outputs
end

--LOG TRAINING
--[TO_DO] We should have separate class, evaluation, logging, etc.
function BQNNAgent:logging()

   if self.sharedModel then
      self.environment._agents_loss = self.environment._agents_loss
           + self.gameLoss
      self.environment._agents_lossRegistered = 1 +
              self.environment._agents_lossRegistered

      if self.environment._agents_lossRegistered ==
              self.environment.agentsNo then

         self.environment._agents_logger:add{['training error/game'] =
                  self.environment._agents_loss
                 / (self.gameMoves * self.environment.agentsNo)}
         self.environment._agents_logger:style{['training error/game'] = '-'}
         self.environment._agents_logger:plot()

         self.environment._agents_loss = 0
         self.environment._agents_lossRegistered = 0
      end
   end

end

function BQNNAgent:saveModel()
   torch.save(self.modelPath, self.model:clearState())
end

function BQNNAgent:chooseAction(newState, previousReward, done)
   local idAction = 0

   newState = self:cast(newState)
   --Reduce chance of rand action each game
   if done then
      self:logging()
      if (self.epsilon > self.epsilonMinimumValue) then
         self.epsilon = self.epsilon * 0.995
      end
   end


   local qValues = self:forwardPass(newState)
   local maxQ, index = torch.max(qValues, 1)
   idAction = index[1]

   if torch.rand(1)[1] < self.epsilon then
      idAction = torch.random(1, self.actions:size(1))
   end

   --RL Train
   if self.gameMoves > 0 then
      local inputs = self.prevState
      local targets = self.prevQ
      targets[self.lastActionId] = previousReward + self.discount * maxQ
      local loss = self:trainNetwork(inputs, targets)
      self.gameLoss = self.gameLoss + loss
   end

   self.prevQ = qValues:clone()
   self.lastActionId = idAction
   self.prevState = newState:clone()
   self.gameMoves = self.gameMoves + 1
   return self.actions[idAction]
end
