require("torch")

local RandomAgent, Parent = torch.class("RandomAgent", "Agent")

function RandomAgent:__init(agentId, environment, opts)
   opts = opts or {}
   self.name = "Random"
   Parent.__init(self, agentId, environment, opts)
   self:info("Random agent initialized")
end

function RandomAgent:chooseAction(newState, previousReward, done)
   return self.actions[1 + torch.bernoulli()]
end
