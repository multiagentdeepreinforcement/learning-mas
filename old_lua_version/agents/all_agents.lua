require("agents.Agent")

local __allAgents = {
   ["random"] = "RandomAgent",
   ["pprandom"] = "PPRandomAgent",
   ["bqnn"] = "BQNNAgent",
   ["dt"] = "DummyTabularAgent",
}

function allAgents()
   local _allAgents = {}
   for agentName, _ in pairs(__allAgents) do
      table.insert(_allAgents, agentName)
   end
   return _allAgents
end

function getAgent(agentName)
   assert(__allAgents[agentName] ~= nil, "Agent not found!")

   require("agents." .. __allAgents[agentName])

   -- Remember the old value
   local __oldVal = __AgentType

   -- Put agent type in global variable __AgentType
   loadstring("__AgentType = " .. __allAgents[agentName])()

   -- Copy agent type to local _AgentType
   local _AgentType = __AgentType

   -- Put back the old value of global varialbe __AgentType
   __AgentType = __oldVal

   -- Return the value of local _AgentType
   return _AgentType
end
