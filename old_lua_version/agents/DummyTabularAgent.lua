require("torch")

local DummyTabularAgent, Parent =
   torch.class("DummyTabularAgent", "Agent")

function DummyTabularAgent:__init(agentId, environment, opts)
   opts = opts or {}
   self.name = "Dummy"
   Parent.__init(self, agentId, environment, opts)

   self.bestAction = {}
   self.bestReward = {}

   self.lastState  = nil
   self.lastAction = nil

   self:info("Dummy tabular agent initialized")
end

function DummyTabularAgent:chooseAction(state, reward)
   local bestReward = self.bestReward
   local bestAction = self.bestAction

   local lastState  = self.lastState
   local lastAction = self.lastAction

   if lastState and lastAction then
      if not bestReward[lastState] or reward > bestReward[lastState] then
         bestReward[lastState] = reward
         bestAction[lastState] = lastAction
      end
   end

   if not bestAction[newState] or torch.bernoulli(0.05) == 1 then
      action = self.actions[1 + torch.bernoulli()]
   else
      action = bestAction[state]
   end

   self.lastState  = state
   self.lastAction = action

   return action
end

function DummyTabularAgent:report()
   self:info("best actions: ", self.bestAction)
   self:info("max rewards: ", self.bestReward)
end
