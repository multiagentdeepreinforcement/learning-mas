require("torch")

local TabularDummyAgent, Parent =
   torch.class("TabularDummyAgent", "Agent")

function RandomAgent:__init(agentId, environment, opts)
   opts = opts or {}
   self.name = "Tabular"
   Parent.__init(self, agentId, environment, opts)
   self:info("Tabular Dummy agent initialized")
end

function RandomAgent:chooseAction(newState, previousReward)
   return self.actions[1 + torch.bernoulli()]
end
