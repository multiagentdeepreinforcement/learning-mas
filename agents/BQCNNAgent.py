# 2017, Andrei N., Tudor B.
from sphinx.addnodes import centered

from .Agent import Agent
from .Agent import Transition
import matplotlib.pyplot as plt

from random import choice
import logging
import os

import numpy as np
import math
import random

import torch
import torch.nn as nn
import torch.optim as optim
import torch.autograd as autograd
import torch.nn.functional as F
import torchvision.transforms as T

from torch.autograd import Variable

class BQCNN_Model(nn.Module):
    def __init__(self, actionNo):
        super(BQCNN_Model, self).__init__()
        self.conv1 = nn.Conv2d(1, 128, kernel_size=3, stride=1)
        self.conv2 = nn.Conv2d(128, 64, kernel_size=3, stride=1)
        self.head = nn.Linear(64, actionNo)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        return self.head(x.view(x.size(0), -1))


class BQCNNAgent(Agent):
    """
    Baseline Agent - Q-Learning with CNN
    """

    def __init__(self, *args, lr = 1e-2, momentum=0, alpha=0.99, lr_eps=1e-8,
                 weight_decay=0, batchSize=100,
                 discount=0.999,
                 eps_start = 0.9, eps_end = 0.05, eps_decay = 200,
                 **kwargs):
        super(BQCNNAgent, self).__init__(*args, **kwargs)

        self.logger.info("On duty...")


        #Load learning architecture parameters
        self.lr = float(lr)
        self.momentum = float(momentum)
        self.alpha = float(alpha)
        self.lr_eps = float(lr_eps)
        self.weight_decay = float(weight_decay)


        self.batchSize = int(batchSize)
        self.discount = float(discount)

        self.eps_start = float(eps_start)
        self.eps_end = float(eps_end)
        self.eps_decay = float(eps_decay)

        self.gameMoves = 0
        self.gameLoss = 0

        self._lastLoss = 0
        self._losses = []

        super().__post_init__()

    def _act(self, observation, reward, done, info):
        """Class code here"""
        sample = random.random()
        eps_threshold = self.eps_end + (self.eps_start - self.eps_end) *\
                                       math.exp(-1. * self._crtStep /
                                                self.eps_decay)
        if sample > eps_threshold:
            q = self._modelClass._model(Variable(observation, volatile=True))
            action = q.data.max(1)[1].cpu()
        else:
            action = torch.LongTensor([[self.action_space.sample()]])

        return action

    def _restart(self):
        pass

    def _epochFinished(self):
        pass

    def _report(self):
        self._losses.append(self._lastLoss)
        self.logger.info("Loss:: {}".format(self._lastLoss))
        self._lastLoss = 0

    def _saveModel(self, *args, **kwargs):
        pass

    def _createLearningArchitecture(self):
        model = BQCNN_Model(self.action_space.n)
        optimizer = optim.RMSprop(model.parameters(), lr=self.lr,
                                  alpha=self.alpha, eps=self.lr_eps,
                                  weight_decay=self.weight_decay)
        criterion = F.smooth_l1_loss
        self._modelClass.loadModel(model, optimizer, criterion)


    def _optimizeModel(self):
        if ((self._crtStep-1) % self.batchSize != 0) or self._crtStep == 1:
            return

        transition = self._memory.sample(self.batchSize)
        BATCH_SIZE = len(transition)
        if BATCH_SIZE <= 0:
            return


        batch = Transition(*zip(*transition))

        state_batch = Variable(torch.cat(batch.state))
        action_batch = Variable(torch.cat(batch.action))
        reward_batch = Variable(torch.cat(batch.reward))
        next_state_values = Variable(torch.zeros(BATCH_SIZE))

        non_final_mask = torch.ByteTensor(batch.done)
        if non_final_mask.any():
            non_final_next_states_t = torch.cat(tuple(s for s in batch.next_state
                                                      if s is not batch.done))\
                .type(self.dtype)
            non_final_next_states = Variable(non_final_next_states_t, volatile=True)
            next_state_values[non_final_mask] = self._modelClass._model(
                non_final_next_states).max(1)[0].cpu()

        if self._useCUDA:
            action_batch = action_batch.cuda()

        expected_state_action_values = (next_state_values * self.discount) + reward_batch
        state_action_values = self._modelClass._model(state_batch).\
            gather(1, action_batch).cpu()

        loss = self._modelClass._criterion(state_action_values,
                                           expected_state_action_values)


        self._lastLoss += loss.data[0]
        self._modelClass._optimizer.zero_grad()
        loss.backward()
        for param in self._modelClass._model.parameters():
            param.grad.data.clamp_(-1, 1)
        self._modelClass._optimizer.step()




