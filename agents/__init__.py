# 2017, Andrei N., Tudor B.

ALL_AGENTS = [
    "random",
    "bqcnn",
    "pp_greedy"
]

def get_agent(name, agentId, action_space, observation_space, env, args):
    # @name         : name of the agent
    # @action_space : set of available actions
    # @args         : string with arguments to configure the agent

    assert name in ALL_AGENTS, "Agent %s is not on duty today." % name

    if name == "random":
        from .RandomAgent import RandomAgent as __Agent
    elif name == "bqcnn":
        from .BQCNNAgent import BQCNNAgent as __Agent
    elif name == "pp_greedy":
        from .PPGreedyAgent import PPGreedyAgent as __Agent

    return __Agent(name, agentId, action_space, observation_space, env, **args)
