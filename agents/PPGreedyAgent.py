# 2017, Andrei N., Tudor B.

from .Agent import Agent
import random
import logging
import numpy as np
import torch

class PPGreedyAgent(Agent):

    def __init__(self, *args, gTargetID=-1, **kwargs):
        super(PPGreedyAgent, self).__init__(*args, **kwargs)

        self.logger.info("On duty...")
        self.targetID = eval(gTargetID)
        self.actions = np.array(self.env.metadata["actions"])

        super().__post_init__()


    def _act(self, observation, reward, done, info):
        action = self.action_space.sample()

        #Assume center positioned agent in observation space
        observation = observation.squeeze(0).squeeze(0).numpy()

        pos = np.where(observation == self.targetID)
        pos = list(zip(*pos))

        if len(pos) > 0:
            posTarget = np.array(pos[0])
            posElemenent = np.array(observation.shape) // 2

            maxDist = float('inf')
            maxAct = -1
            actions_ = list(enumerate(self.actions))
            random.shuffle(actions_)
            for ix, action in actions_:
                dist = np.abs((posTarget) - (posElemenent + action)).sum()
                if dist < maxDist:
                    maxDist = dist
                    maxAct = ix

            if maxAct != -1:
                action = maxAct

        action = torch.LongTensor([[action]])
        return action
