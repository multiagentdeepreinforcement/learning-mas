# 2017, Andrei N., Tudor B.

from .Agent import Agent
from random import choice
import logging
import torch


class RandomAgent(Agent):

    def __init__(self, *args, testArg=0, **kwargs):
        super(RandomAgent, self).__init__(*args, **kwargs)

        self.logger.info("On duty...")
        testArg = int(testArg)

        self.logger.debug("Test log & testArg: " + str(testArg))

        super().__post_init__()


    def _act(self, observation, reward, done, info):
        return torch.LongTensor([[self.action_space.sample()]])
