# 2017, Andrei N., Tudor B.
import sys
import logging
from utils.config_parser import Configurator
import numpy as np

import gym
import numpy as np

configuration = Configurator(configuration=sys.argv[1:])

logger = logging.getLogger("Train")

opts = configuration.getGeneralConf()
optsEnv = configuration.getEnvironmentConf()
agentNames, optsAgents = configuration.getAgentsConf()

n = sum(map(lambda ag: int(ag.split(":")[1]) if ":" in ag else 1,
            opts["agents"].split(",")))

print("{:d} agent{:s} to be spawned.".format(n, "" if n == 1 else "s"))

# ----------------------------------------
# Initialize env

from environments import get_env

env = get_env(opts["environment"], optsEnv)


action_space = env.action_space.spaces
observation_space = env.observation_space.spaces
if type(action_space) != list:
    action_space = [action_space]

assert len(action_space) == n, "Each agent must have its own action space"

# ----------------------------------------
# -- Initialize agents

from agents import get_agent

agents = []
for (agent_name, agent_args, act, obs) in zip(agentNames, optsAgents,
                                              action_space, observation_space):
        newAgent = get_agent(agent_name, len(agents)+1, act, obs,
                             env, agent_args)
        agents.append(newAgent)

# ----------------------------------------
# -- Variables
agentsNo        = int(len(agents))
totalRewards    = {}
epochReward     = {}

totalSteps      = int(opts.get("totalSteps"))
epochSize       = int(opts["epochSize"])
reportEvery     = int(opts["reportEvery"])

epochsNo        = int(totalSteps / epochSize) + 1
crtEpoch        = 0

avgRewards      = np.zeros([agentsNo, epochsNo])
avgMovesPerGame = np.zeros([epochsNo])
epochGameCnt    = 0

saveModelFreq   = int(opts["saveModelFreq"])
sharedModel     = eval(opts["sharedModel"])

# ----------------------------------------
# -- Initialize rendering & logs
import display
render = eval(optsEnv["drawGame"])

def report(step):
    logger.info("_______________REPORT_EPOCH_{}______________".format(crtEpoch))
    logger.info("Epoch:{}\tStep:{}\tAvgMovesPerGame:{}".format(
        crtEpoch, step, avgMovesPerGame[crtEpoch] / epochGameCnt
    ))
    for idA in range(agentsNo):
        logger.info("Epoch:{}\tStep:{}\tAgent:{}\tavgreward:{}".format(
            crtEpoch, step, idA, avgRewards[idA, crtEpoch]
        ))

# ----------------------------------------
# -- Start training

def restartGame():
    observation = env.reset()
    rewards = np.zeros(len(agents))
    done = False
    info = {}

    for agent in agents:
        agent.restart()

    return observation, rewards, done, info

observation, rewards, done, info = restartGame()

for step in range(1, totalSteps + 1):
    #Agents choose actions
    action = []
    for ix, agent in enumerate(agents):
        action.append(agent.act(observation[ix], rewards[ix], done, info))
    # pauseGame = input()

    #Environment take step
    observation, rewards, done, info = env.step(action)

    if done:
        #Send last reward & state to agents (No more moves permitted)
        for ix, agent in enumerate(agents):
            agent.act(observation[ix], rewards[ix], done, info)

        avgMovesPerGame[crtEpoch] = avgMovesPerGame[crtEpoch] + \
                                    env._elapsed_steps
        epochGameCnt += 1
        observation, rewards, done, info = restartGame()

    #Update various information
    for id, reward in enumerate(rewards):
        avgRewards[id, crtEpoch] += reward

    #Report (current epoch might not be over)
    if step % reportEvery == 0:
        report(step)
        for agent in agents: agent.report()

    #Epoch is over
    if step % epochSize == 0:
        avgRewards[:, crtEpoch] /= float(epochSize)
        avgMovesPerGame[crtEpoch] /= float(epochGameCnt)

        if crtEpoch % saveModelFreq == 0:
            if sharedModel:
                agents[0].saveModel(crtEpoch, np.mean(avgRewards[:, crtEpoch]))
            else:
                for ix, agent in enumerate(agents):
                    agent.saveModel(crtEpoch, avgRewards[ix, crtEpoch])

        for ix, agent in enumerate(agents):
            agent.epochFinished()

        epochGameCnt = 0
        crtEpoch += 1

    # Draw game on display (browser-based display server)
    if render:
        env.render()



