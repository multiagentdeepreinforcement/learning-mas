from argparse import ArgumentParser

class TrainArgumentParser(ArgumentParser):

    def __init__(self):

        super(TrainArgumentParser, self).__init__(
            description="Train multi-agent systems"
        )

        self.add_argument(
            '-a', '--agent',
            action="append",
            dest="agent"
        )

        self.add_argument(
            '-aa', '--agent-args',
            action="append",
            dest="agent_args"
        )

        self.add_argument(
            '-e', '--env',
            action="store",
            dest="env"
        )

        self.add_argument(
            '-ea', '--env-args',
            action="store",
            dest="env_args"
        )

        self.add_argument(
            '-f', '--frames-no',
            type=int,
            default=1000,
            action='store',
            dest='frames_no'
        )
