import configparser
import logging
import uuid
import datetime
import os

rootLogger = logging.getLogger()


class Configurator:
    newConf = configparser.ConfigParser(inline_comment_prefixes=("##",))
    newConf.optionxform = lambda option: option

    def __init__(self, configuration=None, default="utils/default_config.ini"):

        #Load Baseline Config
        self.config = configparser.ConfigParser(inline_comment_prefixes=("##",),
                                                allow_no_value=True)
        self.config.optionxform = lambda option: option

        rootLogger.info("Loading default configuration...")
        self.config.read(default)

        if isinstance(configuration, list):
            if len(configuration) == 1:
                configuration = configuration[0]
            elif len(configuration) == 0:
                configuration = None
            else:
                assert False, "List of configurations too big."

        #Load personalized configurations
        if configuration is not None:
            rootLogger.info("Loading special config...")
            if not os.path.exists(configuration):
                # Configuration sent as str(dict)
                confDict = eval(configuration)
                assert isinstance(confDict, dict), \
                    "Configuration sent as argument should be str(dict)"

                self.newConf.read_dict(confDict)
            else:
                #File configuration sent
                self.newConf.read(configuration)

        newConfDict = {s: dict(self.newConf.items(s)) for s in
                   self.newConf.sections()}
        self.config.read_dict(newConfDict)

        #Create experiment folder
        self.env = self.config.get("Common", "environment")
        if len(self.config.get("Common", "environment_aux")) > 0:
            self.env_aux = self.config.get("Common", "environment_aux").split(",")
        else:
            self.env_aux = []

        self.agents = self.config.get("Common", "agents")

        saveFolder_name = self.env + "_" + "_".join(self.agents.split())

        if len(self.config.get("General", "savePrefix")) > 0:
            saveFolder_name = self.config.get("General", "savePrefix") + "_" + \
                              saveFolder_name

        saveFolder = os.path.join(self.config.get("General", "saveFolder"),
                                  saveFolder_name)
        saveFolder = saveFolder.lower()
        saveTest = saveFolder; cnt = 1
        while os.path.isdir(saveTest):
            saveTest = saveFolder + "_" + str(cnt)
            cnt += 1
        saveFolder = saveTest

        os.mkdir(saveFolder)

        #Configure logger
        rootLogger.handlers = []
        logFormatter = logging.Formatter(
            "[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s")

        fileHandler = logging.FileHandler(os.path.join(saveFolder, "logs"))
        fileHandler.setFormatter(logFormatter)
        rootLogger.addHandler(fileHandler)

        if eval(self.config.get("General", "logToStdout")):
            consoleHandler = logging.StreamHandler()
            consoleHandler.setFormatter(logFormatter)
            rootLogger.addHandler(consoleHandler)

        rootLogger.setLevel(eval(self.config.get("General", "verbose")))

        #New configurations
        if not self.newConf.has_section("Common"):
            self.newConf.add_section('Common')

        self.newConf.set("Common", "saveFolder", value=saveFolder)
        self.newConf.set("Common", "experimentID", value=str(uuid.uuid4()))
        self.newConf.set("Common", "experimentDate", value=str(datetime.
                        datetime.now()))

        #Update newConf file with last modifications
        newConfDict = {s: dict(self.newConf.items(s)) for s in
                       self.newConf.sections()}
        self.config.read_dict(newConfDict)


        #Save conf
        with open(os.path.join(saveFolder, 'full_conf.ini'), 'w') as configfile:
            self.config.write(configfile)
        with open(os.path.join(saveFolder, 'conf.ini'), 'w') as configfile:
            self.newConf.write(configfile)

    def getConfiguration(self):
        return self.config

    def getGeneralConf(self):
        cf = dict(self.config["General"])
        cf.update(dict(self.config["Common"]))
        return cf

    def getEnvironmentConf(self):
        cf = dict(self.config["Environment"])
        cf.update(dict(self.config["Common"]))
        cf.update(dict(self.config[self.env]))
        for sub_env in self.env_aux:
            cf[sub_env] = dict(self.config[sub_env])

        return cf

    def getAgentsConf(self):
        agentCfBase = dict(self.config["Agents"])
        agentCfBase.update(dict(self.config["Common"]))

        agentNames = []
        agentConfs = []
        for agentType in self.agents.split(","):
            if ":" not in agentType:
                agentType = agentType + ":1"

            agent_name, cnt = agentType.split(":")
            cnt = int(cnt)
            agent_conf = 'Agent_' + agent_name
            agent_name = agent_name.lower()

            for idx in range(cnt):
                angetConf = dict(agentCfBase)
                angetConf.update(self.config[agent_conf])
                agentNames.append(agent_name)
                agentConfs.append(angetConf)

        assert len(agentConfs) == int(self.config.get("Common", "agentsNo"))
        return agentNames, agentConfs

