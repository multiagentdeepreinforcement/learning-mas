

Example for registering player

gym.envs.register(id='pursuit_problem_v0',
                  entry_point='pursuit_problem.envs:PursuitProblem',
                  kwargs={...},

                  -- Optional
                  trials=100,
                  reward_threshold=None,
                  local_only=False,
                  nondeterministic=False,
                  tags=None,
                  max_episode_steps=None,
                  max_episode_seconds=None,
                  timestep_limit=None)
