# 2017, Andrei N., Tudor B.

import gym
from gym import error, spaces, utils
from gym.utils import seeding

import numpy as np
import logging

import cv2
import display

logger = logging.getLogger(__name__)

"""MAP ELEMENTS"""
MAP_PREDATOR    = "predator"
MAP_PREY        = "prey"
MAP_EMPTY       = "empty"
MAP_WALL        = "wall"

"""MAP IDs"""
#MAP_PREDATORS  = [1..n]
MAP_EMPTY_ID    = 0
MAP_PREY_ID     = -1
MAP_WALL_ID     = -2

PREY_VALID_MOVES        = [[0, 0], [-1, 0], [1, 0], [0, -1], [0, 1], [-1, -1],
                           [-1, 1], [1, 1], [1, -1]]
PREDATOR_VALID_MOVES    = [[0, 0], [-1, 0], [1, 0], [0, -1], [0, 1]]
PREDATOR_WIN_POSITIONS  = [[-1, 0], [1, 0], [0, -1], [0, 1]]

"""Render Setup options (BGR -for opencv)"""
VIEWPORT_W              = 250
VIEWPORT_H              = 250
VIEW_COLOR_PREDATORS    = [[0, 250, 250], [250, 10, 10], [250, 250, 10]]
VIEW_COLOR_PREY         = [0, 250, 0]
VIEW_COLOR_EMPTY        = [[250, 250, 250], [220,220,220]]


"""OBSERVATION Representation Descriptor changes"""
OBS_PREDATORS_ID = 1

def sumTuples(a, b):
    return tuple(map(lambda x, y: x + y, a, b))


class Agent(object):
    def __init__(self, _type, _id, fov):
        self.type = _type
        self.fov = fov
        self.position = np.array([-1,-1], dtype=np.int)
        self.id = _id
        self.maxDist = int(np.round(self.fov / 2.0))

class Map(object):
    def __init__(self, size=6, mapIDs={}, padding=0):
        self.mapSize = np.int(size)
        self.padding = np.int(padding)
        self.localSize = np.int(size + self.padding * 2)
        self.minMapPos = self.padding
        self.maxMapPos = np.int(self.padding + size)

        self.map = np.zeros([self.localSize, self.localSize])

        self.mapIDs = mapIDs

        self.idsLow = min(map(min, mapIDs.values()))
        self.idsHigh = max(map(max, mapIDs.values()))

    def reset(self):
        self.map[:] = self.mapIDs[MAP_WALL][0]
        self.map[self.minMapPos:self.maxMapPos, self.minMapPos:self.maxMapPos] \
            = self.mapIDs[MAP_EMPTY][0]

    def getEmptyPosition(self):
        ix = np.where(self.map == self.mapIDs[MAP_EMPTY][0])
        return list(zip(*ix))

    def positionElement(self, elementId, position):
        self.map[position[0], position[1]] = elementId

    def moveElement(self, whereFrom, newPosition):
        if self.map[newPosition[0],newPosition[1]] == self.mapIDs[MAP_EMPTY][0]:
            self.map[newPosition[0], newPosition[1]] = self.map[whereFrom[0],
                                                                whereFrom[1]]
            self.map[whereFrom[0], whereFrom[1]] = self.mapIDs[MAP_EMPTY][0]
            return newPosition
        return whereFrom

    def elementMakeMove(self, elPosition, move):
        newPosition = elPosition + move
        return self.moveElement(elPosition, newPosition)

    def getMap(self):
        return self.map[self.minMapPos:self.maxMapPos,
               self.minMapPos:self.maxMapPos]

    def getMapView(self, center, padding):
        return self.map[center[0] - padding: center[0] + padding+1,
               center[1] - padding : center[1] + padding+1]

    def isValidMove(self, newPosition):
        return self.map[newPosition[0], newPosition[1]] \
               == self.mapIDs[MAP_EMPTY][0]

class PursuitProblem(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array'],
                "semantics.autoreset": False}

    def __init__(self, agentsNo=2, mapSize=6, reward=1,
                 preyFOV=3,
                 preyValidMoves=PREY_VALID_MOVES,
                 predatorFOV=5,
                 predatorValidMoves=PREDATOR_VALID_MOVES,
                 predatorWinPos=PREDATOR_WIN_POSITIONS,
                 drawGame=False,
                 *args,
                 **kwargs):

        agentsNo = int(agentsNo)
        assert agentsNo >= 2, "Invalid agentsNo"
        self.agentsNo = agentsNo

        mapSize = int(mapSize)
        assert mapSize >= 6, "Invalid mapSize"
        self.mapSize = mapSize

        reward = float(reward)
        self.reward = reward

        preyFOV = int(preyFOV)
        assert preyFOV >= 1 and preyFOV % 2 == 1, "Invalid preyFOV"
        self.preyFOV = preyFOV
        preyValidMoves = eval(preyValidMoves)
        assert isinstance(preyValidMoves, list) and len(preyValidMoves) > 0, \
            "Invalid preyValidMoves"
        self.preyValidMoves = np.array(preyValidMoves)

        predatorFOV = int(predatorFOV)
        assert predatorFOV >= 1 and predatorFOV % 2 == 1, "Invalid predatorFOV"
        self.predatorFOV = predatorFOV
        predatorValidMoves = eval(predatorValidMoves)
        assert isinstance(predatorValidMoves, list) and len(
            predatorValidMoves) > 0, "Invalid predatorValidMoves"
        self.predatorValidMoves = np.array(predatorValidMoves)

        predatorWinPos = eval(predatorWinPos)
        assert isinstance(predatorWinPos, list) and len(
            predatorWinPos) > agentsNo, "Invalid predatorWinPos"
        self.predatorWinPos = np.array(predatorWinPos)

        self.drawGame = eval(drawGame)

        # ---Define local map id range
        self.mapIDs = dict()
        self.mapIDs[MAP_PREDATOR] = np.arange(1, self.agentsNo + 1)
        self.mapIDs[MAP_PREY] = np.array([MAP_PREY_ID])
        self.mapIDs[MAP_WALL] = np.array([MAP_WALL_ID])
        self.mapIDs[MAP_EMPTY] = np.array([MAP_EMPTY_ID])

        # Initiate pray & predators
        self.prey = Agent(MAP_PREY, MAP_PREY_ID, self.preyFOV)

        self.predators = [Agent(MAP_PREDATOR, idp, self.predatorFOV)
                          for idp in self.mapIDs[MAP_PREDATOR]]

        # Initiate map
        self.map = Map(size=self.mapSize, mapIDs=self.mapIDs,
                       padding=max(self.prey.maxDist,
                                   max(x.maxDist for x in self.predators)))

        # Action and observation space definitions
        self.action_space = spaces.Tuple([spaces.Discrete(len(
            self.predatorValidMoves))] * self.agentsNo)
        self.metadata["actions"] = self.predatorValidMoves

        # Save actual actions in metaData



        self.observation_space = spaces.Tuple(
            [spaces.Box(low=self.map.idsLow, high=self.map.idsHigh,
                        shape=(self.predatorFOV, self.predatorFOV))]
            * self.agentsNo)

        # Init variables
        self.gameNo = 0
        self.stepCount = 0

        self._seed()
        self._configure_environment()
        if self.drawGame:
            self._initRenderOptions()

    def _configure_environment(self):
        """Provides a chance for subclasses to override this method"""
        pass

    def _initRenderOptions(self):

        self._viewBkg = np.zeros([self.mapSize, self.mapSize, 3], dtype=np.uint8)
        for idx in np.ndindex(self.mapSize, self.mapSize):
            self._viewBkg[idx] = np.array(VIEW_COLOR_EMPTY[((idx[0]%2)+idx[1])
                                                           % 2], dtype=np.uint8)


        self._viewColors = dict()
        self._viewColors[MAP_PREDATOR] = np.array(VIEW_COLOR_PREDATORS,
                                                  dtype=np.uint8)
        self._viewColors[MAP_PREY] = np.array(VIEW_COLOR_PREY,
                                                  dtype=np.uint8)

        view = cv2.resize(self._viewBkg, (VIEWPORT_W, VIEWPORT_H),
                          interpolation=cv2.INTER_NEAREST)

        self._viewWin = display.image(view,
                                      title='Game:{}_step:{}'
                                      .format(self.gameNo, self.stepCount))

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _step(self, actions):
        self.stepCount += 1
        rewards = np.zeros(self.agentsNo)
        info = {}
        done = False

        self._preyStep()

        assert self.action_space.contains(actions), "Invalid actions"

        for predator, action, ix in zip(self.predators, actions,
                                        range(self.agentsNo)):
            rewards[ix] = self._predatorsStep(predator,
                                              self.predatorValidMoves[action])

        envRewards, gameEnd, infoEnv = self._getEnvReward()
        info.update(infoEnv)
        info["done"] = "Win" if gameEnd else None
        done = done or gameEnd

        rewards += envRewards

        observations = self._getObservations()

        return observations, rewards, done, info

    def _reset(self):
        self.gameNo += 1
        self.stepCount = 0


        winningGame = True

        while winningGame:
            self.map.reset()

            emptyPos = list(map(np.array, self.map.getEmptyPosition()))
            #Position Prey
            nxt = np.random.randint(len(emptyPos))
            self.prey.position = emptyPos.pop(nxt)
            self.map.positionElement(self.prey.id, self.prey.position)

            #Position Predators
            for predator in self.predators:
                nxt = np.random.randint(len(emptyPos))
                predator.position = emptyPos.pop(nxt)
                self.map.positionElement(predator.id, predator.position)

            winningGame = self._isWinningState()

        return self._getObservations()

    def _render(self, mode='human', close=False):
        map = self.getMapView()
        view = self._viewBkg.copy()


        view[map == self.prey.id] = self._viewColors[MAP_PREY]
        for i, predator in enumerate(self.predators):
            view[map == predator.id] = self._viewColors[MAP_PREDATOR][i]

        view = cv2.resize(view, (VIEWPORT_W, VIEWPORT_H),
                          interpolation=cv2.INTER_NEAREST)

        #Change image for display settings
        view = cv2.cvtColor(view, cv2.COLOR_BGR2RGB)
        if mode == "human":
            view2 = cv2.flip(view, 0)
            self._viewWin = display.image(view2,
                                          title='Game:{}_step:{}'.
                                          format(self.gameNo, self.stepCount),
                                          win=self._viewWin)
        elif mode is 'rgb_array':
            return view
        else:
            logger.warning("No render mode for {}".format(mode))


    def _preyStep(self):
        """Prey algorithm for movement (greedy max distant move)"""

        visiblePredator = []

        #Get predators in field of view
        for predator in self.predators:
            dist = np.abs(predator.position - self.prey.position)
            if (dist < self.prey.maxDist).all():
                visiblePredator.append(predator)

        maxDistFromPredators = 0
        maxMove = None

        for move in self.preyValidMoves[np.random.permutation(
                self.preyValidMoves.shape[0])]:
            newPos = move + self.prey.position
            if self.map.isValidMove(newPos):
                dist = 0
                for predator in visiblePredator:
                    dist += np.abs(predator.position - newPos).sum()

                if dist > maxDistFromPredators:
                    maxMove = move
                    maxDistFromPredators = dist

        if maxMove is not None:
            self.prey.position = self.map.elementMakeMove(self.prey.position,
                                                          maxMove)

    def _predatorsStep(self, predator, move):
        """Must return individual reward for predator given the move made"""

        reward = 0
        predator.position = self.map.elementMakeMove(predator.position, move)
        return reward

    def _getEnvReward(self):
        """Gets global rewards (depending on entire env)"""

        rewards = np.zeros(self.agentsNo)
        end_of_game = False
        info = {}

        win = self._isWinningState()

        if win:
            rewards.fill(self._getWinReward())

        end_of_game = end_of_game or win
        return rewards, end_of_game, info

    def _isWinningState(self):
        """Check winning position"""

        winPosPredatorCnt = 0
        winPos = self.prey.position + self.predatorWinPos
        for predator in self.predators:
            if any((winPos[:] == predator.position).all(1)):
                winPosPredatorCnt += 1
        return winPosPredatorCnt == self.agentsNo

    def _getWinReward(self):
        """Get Winning Position"""

        reward = self.reward
        return reward

    def _getObservations(self):
        """Presents a "ID_MAPS"
        Binary individual map State representation from the view of the predator
        1x{FOVxFOV} -- -2 wall, -1 prey, 0 empty, 1 predators
        """

        obs = np.zeros([self.agentsNo, self.predatorFOV, self.predatorFOV])
        for ix, predator in enumerate(self.predators):
            obs[ix] = self.map.getMapView(predator.position, predator.maxDist)
            obs[ix, predator.maxDist, predator.maxDist] = self.mapIDs[MAP_EMPTY]

        #Observation Representation modifications
        for elID in self.mapIDs[MAP_PREDATOR]:
            obs[obs == elID] = OBS_PREDATORS_ID

        return obs

    def getMapView(self):
        return self.map.getMap()


"""JUST FOR TESTING (TEMP)"""
if __name__ == "__main__":

    from environments import pursuit_problem
    import gym
    gym_id = 0

    gym_id += 1
    gym_name= 'pp-v' + str(gym_id)
    # reload(pursuit_problem)
    # reload(gym)
    gym.envs.registration.register(
        id=gym_name,
        entry_point='environments.pursuit_problem.envs:PursuitProblem',
    )

    env = gym.make(gym_name)

    for i_episode in range(20):
        observation = env.reset()
        for t in range(100):
            print("___OBSERVATION____")
            print(observation)
            action = env.action_space.sample()
            print("___ACTION____")
            print(action)
            env.render()
            input("Press Enter to continue...")
            observation, reward, done, info = env.step(action)
            print("REWARDS::{}".format(reward))
            if done:
                print("Episode finished after {} timesteps".format(t + 1))
                break




