# 2017, Andrei N., Tudor B.
from gym.envs.registration import register
import gym

from environments.wrappers.env_minMaxScaling import ENV_MinMaxScalingWrapper

ALL_ENVIRONMENTS = [
    "PP_"
]

def get_env(name, env_args):
    # @name         : name of the environment
    # @args         : string with arguments to configure the environment

    assert name in ALL_ENVIRONMENTS, "I don't know this %s env." % name

    #TODO Parse env args
    env_version = 0
    env_name = name + "-v" + str(env_version)

    if name == "PP_":
        from .pursuit_problem.envs import PursuitProblem
        register(
            id=env_name,
            entry_point='environments.pursuit_problem.envs:PursuitProblem',
            kwargs=env_args
        )

    env = gym.make(env_name)

    #Wrapper for max episode limit (steps/time)


    env.metadata["wrappers"] = []

    for wrapper in env_args["environment_aux"].split(","):
        if wrapper in env.metadata["wrappers"]:
            continue

        env.metadata["wrappers"].append(wrapper)
        #List here possible wrappers
        if wrapper == "Monitor":
            env = gym.wrappers.Monitor(env, env_args["saveFolder"], force=True)
        if wrapper == "ENV_MinMaxScaling":
            env = ENV_MinMaxScalingWrapper(env)
        else:
            env.metadata["wrappers"].pop(len(env.metadata["wrappers"])-1)

    env = gym.wrappers.TimeLimit(env, max_episode_steps=int(env_args["maxMoves"]))

    return env
