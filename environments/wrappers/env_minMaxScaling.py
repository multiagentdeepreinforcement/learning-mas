import gym
import numpy as np


class ENV_MinMaxScalingWrapper(gym.Wrapper):
    """
        General wrapper to normalize observation space
    """

    def __init__(self, env):
        super(ENV_MinMaxScalingWrapper, self).__init__(env)
        #TODO Generalize redo observation space with low=-1, high=1
        self._obsMin = np.min(self.observation_space.spaces[0].low)
        self._obsMax = np.max(self.observation_space.spaces[0].high)
        self._obsRange = float(self._obsMax - self._obsMin)

    def _step(self, action):
        observation, reward, done, info = self.env.step(action)
        observation = (observation - self._obsMin)/self._obsRange

        return observation, reward, done, info

